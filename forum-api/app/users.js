const express = require('express');
const User = require("../models/User");

const router = express.Router();

router.post('/', async(req, res) => {
    try{
        const userData = {
            username: req.body.username,
            password: req.body.password,
        };

        const user = new User(userData);
        user.generateToken();
        await user.save();
        res.send(user);
    }catch(e){
        res.status(400).send(e)
    }
});

router.post('/sessions', async(req, res) => {
    try{
        const user = await User.findOne({username: req.body.username});
        if(!user) return res.status(401).send({message: 'Credentials are wrong'});

        const isMatch = await user.checkPassword(req.body.password);
        if(!isMatch) return res.status(401).send({message: 'Credentials are wrong'});

        user.generateToken();
        await user.save({validateBeforeSave: false});
        console.log(user, '32')
        res.send(user);
    }catch(e){
        res.status(400).send(e)
    }
});

router.delete('/sessions', async(req, res) => {
    try{
        const auth = req.get('Authorization');
        const success = {message: 'Success'};

        if(!auth) return res.send(success);

        const user = await User.findOne({auth});
        if(!user) return res.send(success);

        user.generateToken();
        await user.save({validateBeforeSave: false});
        res.send(success)
    }catch(e){
        res.sendStatus(500);
    }
});

module.exports = router;