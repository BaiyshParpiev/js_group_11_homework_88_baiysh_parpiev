const express = require('express');
const Comment = require('../models/Comment');
const auth = require('../middleware/auth');

const router = express.Router();

router.get('/', auth, async(req,res) => {
    try{
        const query = {};
        if(req.query.forum){
            query.forum = req.query.forum
        }
        const comments = await Comment.find(query)
            .populate('author', 'username')
        res.send(comments)
    }catch{
        res.sendStatus(500);
    }
});

router.post('/', auth, async(req, res) => {
   try{
       const commentData = {
           text: req.body.text,
           forum: req.body.forum,
           author: req.user._id,
       };

       const comment = new Comment(commentData);
       await comment.save();
       await comment.populate('author', 'username');
       res.send(comment)
   }catch(e){
       res.send(400).send(e)
   }
});


router.delete('/:id', auth, async (req, res) => {
    try{
        const {id} = req.params;
        const forum = await Comment.findById(id)
        if (!forum) return res.status(404).send(`No comment with id: ${id}`);
        await Comment.findByIdAndRemove(id);
        res.send({message: "Forum deleted successfully"});
    }catch{
        res.sendStatus(500);
    }
});

module.exports = router;