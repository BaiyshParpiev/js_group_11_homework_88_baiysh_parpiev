const express = require('express');
const Forum = require('../models/Forum')
const multer = require('multer');
const config = require('../config');
const path = require('path');
const {nanoid} = require('nanoid');
const auth = require('../middleware/auth');

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, config.uploadPath);
    },
    filename: (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname));
    }
});

const upload = multer({storage});

const router = express.Router();

router.get('/', async (req, res) => {
    try {
        const forums = await Forum.find().populate('author', 'username').sort([['datetime', -1]]);
        res.send(forums);
    } catch (e) {
        res.sendStatus(500);
    }
});

router.get('/:id', async (req, res) => {
    try {
        const forum = await Forum.findById(req.params.id);
        if (forum) {
            res.send(forum);
        } else {
            res.status(400).send({error: 'Forum not found'});
        }
    } catch {
        res.sendStatus(500);
    }
})

router.post('/', [auth, upload.single('image')], async (req, res) => {
    try {
        const formData = {
            title: req.body.title,
            author: req.user._id,
            datetime: new Date().toISOString(),
            description: req.body.description || null,
        };

        if (req.file) {
            formData.image = 'uploads/' + req.file.filename;
        }

        const forums = new Forum(formData);

        await forums.save();
        res.send(forums);
        res.status(400).send({error: 'Data not valid'});
    } catch (e) {
        console.log(e)
        res.status(400).send({error: 'Data not valid'});
    }
});

router.put('/:id', [auth, upload.single('image')], async (req, res) => {
    try {
        const forum = await Forum.findById(req.params.id);
        if (!forum) return res.status(404).send({message: 'Forum not found'});


        const formData = {
            title: req.body.title,
            description: req.body.description || null,
        };

        if (req.file) {
            formData.image = 'uploads/' + req.file.filename;
        }

        await Forum.findByIdAndUpdate(req.params.id, formData, { new: true });
        res.send(formData);
    } catch{
        res.status(400).send({message: 'Data not valid'});
    }
});


router.delete('/:id', auth, async(req, res) => {
    try{

        const {id} = req.params;
        console.log(id)
        const forum = await Forum.findById(id)
        if (!forum) return res.status(404).send(`No forum with id: ${id}`);
        await Forum.findByIdAndRemove(id);
        res.send({message: "Forum deleted successfully"});
    }catch{
        res.sendStatus(500);
    }
});

module.exports = router;