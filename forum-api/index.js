const express = require('express');
const mongoose = require('mongoose');
const cors = require('cors');
const exitHook = require('async-exit-hook');
const config = require('./config');
const users = require('./app/users');
const forums = require('./app/forums');
const comments = require('./app/comments');


const app = express();
app.use(express.json());
app.use(cors());
app.use(express.static('public'));

app.use('/users', users);
app.use('/forums', forums);
app.use('/comments', comments)

const port = 8000;

const run = async() => {
    await mongoose.connect(config.db.url);

    app.listen(port, () => {
        console.log(`Server started on ${port} port!`)
    });


    exitHook(() => {
        console.log('Exiting');
        mongoose.disconnect();
    })
};

run().catch(e => console.log(e))