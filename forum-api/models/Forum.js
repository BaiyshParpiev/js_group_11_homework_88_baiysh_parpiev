const mongoose = require('mongoose');
const idValidator = require('mongoose-id-validator');

const ForumSchema = new mongoose.Schema({
    author: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User',
        required: true,
    },
    title: {
        type: String,
        required: true,
    },
    description: {
        type: String,
        required: function(){
            return !this.image;
        }
    },
    image: {
        type: String,
        required: function(){
            return !this.description;
        }
    },
    datetime: {
        type: String,
        required: true,
    }
});

ForumSchema.plugin(idValidator);

const Forum = mongoose.model('Forum', ForumSchema);

module.exports = Forum;