const mongoose = require('mongoose');
const idValidator = require('mongoose-id-validator');

const CommentSchema = new mongoose.Schema({
    author: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User',
        required: true,
    },
    text: {
        type: String,
        required: true,
    },
    forum: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Forum',
        required: true,
    },
});

CommentSchema.plugin(idValidator);

const Comment = mongoose.model('Comment', CommentSchema);

module.exports = Comment;