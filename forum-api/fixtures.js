const mongoose = require('mongoose');
const {nanoid} = require('nanoid');
const config = require('./config');
const Forum = require("./models/Forum");
const Comment = require("./models/Comment");
const User = require("./models/User");

const run = async () => {
  await mongoose.connect(config.db.url);

  const collections = await mongoose.connection.db.listCollections().toArray();

  for (const coll of collections) {
    await mongoose.connection.db.dropCollection(coll.name);
  }

  const [firstUser, secondUser] = await User.create({
    username: 'admin',
    password: 'admin',
    token: nanoid(),
  }, {
    username: 'root',
    password: 'root',
    token: nanoid(),
  });

  const [firstForum, secondForum] = await Forum.create({
    title: 'New year 2022',
    description: 'Big plans to realize aims',
    image: 'fixtures/second.jpg',
    datetime: new Date().toISOString(),
    author: firstUser,
  }, {
    title: 'New chances',
    description: 'Hard work',
    image: 'fixtures/first.jpg',
    datetime: new Date().toISOString(),
    author: secondUser,
  });



  await Comment.create({
    text: 'it is possible',
    forum: firstForum,
    author: firstUser,
  }, {
    text: 'Just do it',
    forum: secondForum,
    author: secondUser,
  });


  await mongoose.connection.close();
};

run().catch(console.error);