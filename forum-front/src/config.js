const axios = require("axios");

export const axiosApi = axios.create({
    baseURL: 'http://localhost:8000'
});