import React from 'react';
import Layout from "./components/UI/Layout/Layout";
import {Route, Switch} from "react-router-dom";
import Forums from "./containers/Forums/Forums";
import Forum from "./containers/Forum/Forum";
import NewForum from "./containers/NewForum/NewForum";
import Register from "./containers/Register/Register";
import Login from "./containers/Login/Login";

const App = () => {
    return (
        <Layout>
            <Switch>
                <Route path="/" exact component={Forums}/>
                <Route path="/newForum" component={NewForum}/>
                <Route path="/forums/:id" component={Forum}/>
                <Route path="/signup" component={Register}/>
                <Route path="/signin" component={Login}/>
            </Switch>
        </Layout>
    );
};

export default App;