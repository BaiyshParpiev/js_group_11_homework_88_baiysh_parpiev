import React from 'react';
import {Link} from "react-router-dom";
import {Button} from "@material-ui/core";

const AnonymousMenu = () => {
  return (
    <>
      <Button component={Link} to="/signup" color="inherit">Sign up</Button>
      <Button component={Link} to="/signin" color="inherit">Sign in</Button>
    </>
  );
};

export default AnonymousMenu;