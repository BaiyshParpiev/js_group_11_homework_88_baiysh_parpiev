import {
    Card,
    CardActions,
    CardContent,
    CardHeader,
    CardMedia,
    Grid,
    IconButton,
    makeStyles,
    Typography
} from "@material-ui/core";
import ArrowForwardIcon from "@material-ui/icons/ArrowForward";
import {Link} from "react-router-dom";
import comment from '../../assets/images/comment.png';


const useStyles = makeStyles({
    card: {
        height: '100%'
    },
    media: {
        height: 0,
        paddingTop: '56.25%'
    }
})

const ForumItem = ({title, id, image, datetime, author}) => {
    const classes = useStyles();

    let cardImage = comment;

    if (image) {
        cardImage = "http://localhost:8000/" + image;
    }

    return (
        <Grid item xs={12} sm={6} md={6} lg={4}>
            <Card className={classes.card}>
                <CardHeader title={title}/>
                <CardMedia
                    image={cardImage}
                    title={title}
                    className={classes.media}
                />
                <CardContent>
                    <Typography variant="subtitle1">
                        {author}
                    </Typography>
                </CardContent>
                <CardContent>
                    <Typography variant="subtitle2">
                        {datetime}
                    </Typography>
                </CardContent>
                <CardActions>
                    <IconButton component={Link} to={'/forums/' + id}>
                        <ArrowForwardIcon />
                    </IconButton>
                </CardActions>
            </Card>
        </Grid>
    );
};


export default ForumItem;