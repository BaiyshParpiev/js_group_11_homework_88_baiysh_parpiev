import React from 'react';
import {Grid, TextField} from "@material-ui/core";

const FormElement = ({label, name, value, onChange, required, error, autoComplete, type}) => {
  return (
    <Grid item xs={12}>
      <TextField
        fullWidth
        variant="outlined"
        type={type}
        required={required}
        autoComplete={autoComplete}
        label={label}
        name={name}
        value={value}
        onChange={onChange}
        error={Boolean(error)}
        helperText={error}
      />
    </Grid>
  );
};


export default FormElement;