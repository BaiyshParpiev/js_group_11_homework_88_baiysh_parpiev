import React from 'react';
import {Typography} from "@material-ui/core";
import {useDispatch} from "react-redux";
import {createForum} from "../../store/actions/forumsActions";
import Form from "../../components/Form/Form";

const NewForum = ({history}) => {
    const dispatch = useDispatch();

    const onSubmit = async data => {
        await dispatch(createForum(data));
        history.replace('/');
    };

    return (
        <>
            <Typography variant="h4">New Forum</Typography>
            <Form
                onSubmit={onSubmit}
            />
        </>
    );
};

export default NewForum;