import React, {useEffect} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {Box, Button, CardMedia, Grid, makeStyles, Paper, Typography} from "@material-ui/core";
import {fetchForum} from "../../store/actions/forumsActions";
import comment from '../../assets/images/comment.png';
import {createComment, deleteComment, fetchComments} from "../../store/actions/commentsActions";
import CommentsForm from "../../components/Form/CommentsForm/CommentsForm";

const useStyles = makeStyles(theme => ({
    media: {
        height: 0,
        paddingTop: '56.25%',
        backgroundColor: 'rgba(0, 0, 0, 0.5)',
        backgroundBlendMode: 'darken',
    },
}));
const Forum = ({match}) => {
    const classes = useStyles();
    const dispatch = useDispatch();
    const {forum} = useSelector(state => state.forums);
    const {user} = useSelector(state  => state.users)
    const {comments} = useSelector(state => state.comments)

    useEffect(() => {
        dispatch(fetchForum(match.params.id));
    }, [dispatch, match.params.id]);

    const commentsHandler = e => {
        e.preventDefault();
        dispatch(fetchComments(forum._id));
    };

    const createComments = data => {
        dispatch(createComment(data))
    }

    const onRemoveHandler = id => {
        dispatch(deleteComment(id))
    }

    return forum && (
        <Paper component={Box} p={4}>
            <Paper component={Box} p={3}>
                <Grid item>
                    <CardMedia className={classes.media}
                               image={forum.image ? "http://localhost:8000/" + forum.image : comment}/>
                    <Grid container direction="column" spacing={3}>
                        <Grid item>
                            <Grid container justifyContent="space-between">
                                <Grid item>
                                    <Typography variant='h4'>
                                        Title: {forum.title}
                                    </Typography>
                                </Grid>
                                <Grid item>
                                    <Typography variant='subtitle2'>
                                        {forum.datetime}
                                    </Typography>
                                </Grid>
                            </Grid>
                        </Grid>
                        <Grid item>
                            <Typography variant='body1'>
                                Author: {forum.author.username}
                            </Typography>
                        </Grid>
                        <Grid item>
                            <Typography variant='body1'>
                                Info: {forum.description}
                            </Typography>
                        </Grid>
                    </Grid>
                </Grid>
            </Paper>
            {comments && user && (
                <Paper component={Box} p={3}>
                    <Grid item>
                        <Button color="primary" onClick={commentsHandler}>Comment</Button>
                    </Grid>
                    <Grid container direction="column" spacing={4}>
                        {comments && comments.map(c => (
                            <Grid item key={c._id}>
                                <Paper component={Box} p={3}>
                                    <Grid container flexdirection="row" justifyContent="space-between"
                                          alignItems="center">
                                        <Typography variant='subtitle1'>
                                            {c.author?.username}
                                        </Typography>
                                        <Typography variant='subtitle2'>
                                            {c.text}
                                        </Typography>
                                        <Button size="small" color="primary" onClick={() => onRemoveHandler(c._id)}>
                                            Delete
                                        </Button>
                                    </Grid>
                                </Paper>
                            </Grid>
                        ))}
                    </Grid>
                    <CommentsForm onSubmit={createComments} id={match.params.id}/>
                </Paper>
            )}
        </Paper>
    );
};

export default Forum;