import React, {useEffect} from 'react';
import {CircularProgress, Grid, Typography} from "@material-ui/core";
import {useDispatch, useSelector} from "react-redux";
import {fetchForums} from "../../store/actions/forumsActions";
import ForumItem from '../../components/ForumItem/ForumItem';

const Forums = () => {
    const dispatch = useDispatch();
    const {forums, fetchLoading} = useSelector(state => state.forums);

    useEffect(() => {
        dispatch(fetchForums());
    }, [dispatch]);

    return (
        <Grid container direction="column" spacing={2}>
            <Grid item container justifyContent="space-between" alignItems="center">
                <Grid item>
                    <Typography variant="h4">Forums</Typography>
                </Grid>
            </Grid>
            <Grid item>
                <Grid item container direction="row" spacing={1}>
                    {fetchLoading ? (
                        <Grid container justifyContent="center" alignItems="center">
                            <Grid item>
                                <CircularProgress/>
                            </Grid>
                        </Grid>
                    ) : forums.map(f => (
                        <ForumItem
                            key={f._id}
                            id={f._id}
                            title={f.title}
                            description={f.description}
                            image={f.image}
                            datetime={f.datetime}
                            author={f.author?.username}
                        />
                    ))}
                </Grid>
            </Grid>
        </Grid>
    );
};

export default Forums;