import {
    CREATE_COMMENT_FAILURE,
    CREATE_COMMENT_REQUEST,
    CREATE_COMMENT_SUCCESS,
    DELETE_COMMENT_FAILURE,
    DELETE_COMMENT_REQUEST,
    DELETE_COMMENT_SUCCESS,
    FETCH_COMMENTS_FAILURE,
    FETCH_COMMENTS_REQUEST,
    FETCH_COMMENTS_SUCCESS
} from "../actions/commentsActions";

const initialState = {
    comments: [],
    fetchLoading: false,
    fetchError: null,
};

const commentsReducer = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_COMMENTS_REQUEST:
            return {...state, fetchLoading: true, fetchError: null};
        case FETCH_COMMENTS_SUCCESS:
            return {...state, fetchLoading: false, fetchError: null, comments: action.payload};
        case FETCH_COMMENTS_FAILURE:
            return {...state, fetchLoading: false, fetchError: action.payload};
        case CREATE_COMMENT_REQUEST:
            return {...state, fetchLoading: true, fetchError: null};
        case CREATE_COMMENT_SUCCESS:
            return {...state, fetchLoading: false, fetchError: null, comments: [...state.comments, action.payload]};
        case CREATE_COMMENT_FAILURE:
            return {...state, fetchLoading: false, fetchError: action.payload};
        case DELETE_COMMENT_REQUEST:
            return {...state, fetchLoading: true, fetchError: null};
        case DELETE_COMMENT_SUCCESS:
            return {...state,
                fetchLoading: false,
                fetchError: null,
                comments: state.comments.filter(p => p._id !== action.payload),
                };
        case DELETE_COMMENT_FAILURE:
            return {...state, fetchLoading: false, fetchError: action.payload};
        default:
            return state;
    }
};

export default commentsReducer;