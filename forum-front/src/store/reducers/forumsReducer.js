import {
    CREATE_FORUM_FAILURE,
    CREATE_FORUM_REQUEST, CREATE_FORUM_SUCCESS,
    DELETE_FORUM_FAILURE,
    DELETE_FORUM_REQUEST, DELETE_FORUM_SUCCESS,
    FETCH_FORUM_FAILURE,
    FETCH_FORUM_REQUEST,
    FETCH_FORUM_SUCCESS,
    FETCH_FORUMS_FAILURE,
    FETCH_FORUMS_REQUEST,
    FETCH_FORUMS_SUCCESS, UPDATE_FORUM_FAILURE, UPDATE_FORUM_REQUEST, UPDATE_FORUM_SUCCESS
} from "../actions/forumsActions";

const initialState = {
    forums: [],
    forum: null,
    fetchLoading: false,
    fetchError: null,
};

const forumsReducer = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_FORUMS_REQUEST:
            return {...state, fetchLoading: true, fetchError: null};
        case FETCH_FORUMS_SUCCESS:
            return {...state, fetchLoading: false, fetchError: null, forums: action.payload};
        case FETCH_FORUMS_FAILURE:
            return {...state, fetchLoading: false, fetchError: action.payload};
        case FETCH_FORUM_REQUEST:
            return {...state, fetchLoading: true, fetchError: null};
        case FETCH_FORUM_SUCCESS:
            return {...state, fetchLoading: false, fetchError: null, forum: action.payload};
        case FETCH_FORUM_FAILURE:
            return {...state, fetchLoading: false, fetchError: action.payload};
        case UPDATE_FORUM_REQUEST:
            return {...state, fetchLoading: true, fetchError: null};
        case UPDATE_FORUM_SUCCESS:
            return {
                ...state,
                fetchLoading: false,
                fetchError: null,
                forums: state.forums.map(f => f._id === action.payload._id ? action.payload : f),
            };
        case UPDATE_FORUM_FAILURE:
            return {...state, fetchLoading: false, fetchError: action.payload};
        case CREATE_FORUM_REQUEST:
            return {...state, fetchLoading: true, fetchError: null};
        case CREATE_FORUM_SUCCESS:
            return {
                ...state,
                fetchLoading: false,
                fetchError: null,
                forums: [...state.forums, action.payload],
            };
        case CREATE_FORUM_FAILURE:
            return {...state, fetchLoading: false, fetchError: action.payload};
        case DELETE_FORUM_REQUEST:
            return {...state, fetchLoading: true, fetchError: null};
        case DELETE_FORUM_SUCCESS:
            return {
                ...state,
                fetchLoading: false,
                fetchError: null,
                forums: state.forums.filter(p => p._id !== action.payload),
            };
        case DELETE_FORUM_FAILURE:
            return {...state, fetchLoading: false, fetchError: action.payload};
        default:
            return state;
    }
};

export default forumsReducer;