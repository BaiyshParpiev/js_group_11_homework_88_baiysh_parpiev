import {axiosApi} from "../../config";

export const FETCH_FORUMS_REQUEST = 'FETCH_FORUMS_REQUEST';
export const FETCH_FORUMS_SUCCESS = 'FETCH_FORUMS_SUCCESS';
export const FETCH_FORUMS_FAILURE = 'FETCH_FORUMS_FAILURE';

export const FETCH_FORUM_REQUEST = 'FETCH_FORUM_REQUEST';
export const FETCH_FORUM_SUCCESS = 'FETCH_FORUM_SUCCESS';
export const FETCH_FORUM_FAILURE = 'FETCH_FORUM_FAILURE';

export const CREATE_FORUM_REQUEST = 'CREATE_FORUM_REQUEST';
export const CREATE_FORUM_SUCCESS = 'CREATE_FORUM_SUCCESS';
export const CREATE_FORUM_FAILURE = 'CREATE_FORUM_FAILURE';

export const UPDATE_FORUM_REQUEST = 'UPDATE_FORUM_REQUEST';
export const UPDATE_FORUM_SUCCESS = 'UPDATE_FORUM_SUCCESS';
export const UPDATE_FORUM_FAILURE = 'UPDATE_FORUM_FAILURE';

export const DELETE_FORUM_REQUEST = 'DELETE_FORUM_REQUEST';
export const DELETE_FORUM_SUCCESS = 'DELETE_FORUM_SUCCESS';
export const DELETE_FORUM_FAILURE = 'DELETE_FORUM_FAILURE';

const fetchForumsRequest = () => ({type: FETCH_FORUMS_REQUEST});
const fetchForumsSuccess = data => ({type: FETCH_FORUMS_SUCCESS, payload: data});
const fetchForumsFailure = error => ({type: FETCH_FORUMS_FAILURE, payload: error});

const fetchForumRequest = () => ({type: FETCH_FORUM_REQUEST});
const fetchForumSuccess = data => ({type: FETCH_FORUM_SUCCESS, payload: data});
const fetchForumFailure = error => ({type: FETCH_FORUM_FAILURE, payload: error});

const createForumRequest = () => ({type: CREATE_FORUM_REQUEST});
const createForumSuccess = data => ({type: CREATE_FORUM_SUCCESS, payload: data});
const createForumFailure = error => ({type:CREATE_FORUM_FAILURE, payload: error});

const updateForumRequest = () => ({type: UPDATE_FORUM_REQUEST});
const updateForumSuccess = data => ({type: UPDATE_FORUM_SUCCESS, payload: data});
const updateForumFailure = error => ({type: UPDATE_FORUM_FAILURE, payload: error});

const deleteForumRequest = () => ({type: DELETE_FORUM_REQUEST});
const deleteForumSuccess = id => ({type: DELETE_FORUM_SUCCESS, payload: id});
const deleteForumFailure = error => ({type: DELETE_FORUM_FAILURE, payload: error});


export const fetchForums = () => async dispatch => {
    try{
        dispatch(fetchForumsRequest());
        const {data} = await axiosApi.get('/forums');
        dispatch(fetchForumsSuccess(data));
    }catch(e){
        dispatch(fetchForumsFailure(e));
    }
};

export const fetchForum = id => async dispatch => {
    try{
        dispatch(fetchForumRequest());
        const {data} = await axiosApi.get('/forums/' + id);
        dispatch(fetchForumSuccess(data));
    }catch(e){
        dispatch(fetchForumFailure(e));
    }
};
export const createForum = d => async dispatch => {
    try{
        dispatch(createForumRequest());
        const {data} = await axiosApi.post('/forums', d);
        dispatch(createForumSuccess(data));
    }catch(e){
        dispatch(createForumFailure(e));
    }
};

export const updateForum = id => async dispatch => {
    try{
        dispatch(updateForumRequest());
        const {data} = await axiosApi.post('/forums/' + id);
        dispatch(updateForumSuccess(data));
    }catch(e){
        dispatch(updateForumFailure(e));
    }
};

export const deleteForum = id => async dispatch => {
    try{
        dispatch(deleteForumRequest());
        const {data} = await axiosApi.delete('/forums/' + id);
        dispatch(deleteForumSuccess(data));
    }catch(e){
        dispatch(deleteForumFailure(e));
    }
};




