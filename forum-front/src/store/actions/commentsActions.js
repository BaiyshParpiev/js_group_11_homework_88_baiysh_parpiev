import {axiosApi} from "../../config";

export const FETCH_COMMENTS_REQUEST = 'FETCH_COMMENTS_REQUEST';
export const FETCH_COMMENTS_SUCCESS = 'FETCH_COMMENTS_SUCCESS';
export const FETCH_COMMENTS_FAILURE = 'FETCH_COMMENTS_FAILURE';

export const CREATE_COMMENT_REQUEST = 'CREATE_COMMENT_REQUEST';
export const CREATE_COMMENT_SUCCESS = 'CREATE_COMMENT_SUCCESS';
export const CREATE_COMMENT_FAILURE = 'CREATE_COMMENT_FAILURE';

export const DELETE_COMMENT_REQUEST = 'DELETE_COMMENT_REQUEST';
export const DELETE_COMMENT_SUCCESS = 'DELETE_COMMENT_SUCCESS';
export const DELETE_COMMENT_FAILURE = 'DELETE_COMMENT_FAILURE';


const fetchCommentsRequest = () => ({type: FETCH_COMMENTS_REQUEST});
const fetchCommentsSuccess = data => ({type: FETCH_COMMENTS_SUCCESS, payload: data});
const fetchCommentsFailure = error => ({type: FETCH_COMMENTS_FAILURE, payload: error});

const createCommentRequest = () => ({type: CREATE_COMMENT_REQUEST});
const createCommentSuccess = data => ({type: CREATE_COMMENT_SUCCESS, payload: data});
const createCommentFailure = error => ({type:CREATE_COMMENT_FAILURE, payload: error});

const deleteCommentRequest = () => ({type: DELETE_COMMENT_REQUEST});
const deleteCommentSuccess = id => ({type: DELETE_COMMENT_SUCCESS, payload: id});
const deleteCommentFailure = error => ({type: DELETE_COMMENT_FAILURE, payload: error});



export const fetchComments = id => async dispatch => {
    try{
        dispatch(fetchCommentsRequest());
        const {data} = await axiosApi.get('/comments?forum=' + id);
        dispatch(fetchCommentsSuccess(data));
    }catch(e){
        dispatch(fetchCommentsFailure(e));
    }
};

export const createComment = (c) => async dispatch => {
    try{
        dispatch(createCommentRequest());
        const {data} = await axiosApi.post('/comments', c);
        dispatch(createCommentSuccess(data));
    }catch(e){
        dispatch(createCommentFailure(e));
    }
};

export const deleteComment = id => async dispatch => {
    try{
        dispatch(deleteCommentRequest());
        await axiosApi.delete('/comments/' + id);
        dispatch(deleteCommentSuccess(id));
    }catch(e){
        dispatch(deleteCommentFailure(e));
    }
};